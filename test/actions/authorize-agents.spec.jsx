import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import Actions from '../../src/actions'
import APP_ACTIONS from '../../src/constants/app-actions'
import nock from 'nock'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('authorize-agents actions', () => {
    afterEach(() => {
        nock.cleanAll()
    })


    it('creates authorize agent when post /authorize_agents suceeded', () => {
        let nock_response = {authorize_agent:{number:"AA481657234",wechat_ticket: "ticket1"},meta:{}};
        nock(`${process.env.REACT_APP_AMS_API_BASE}`).persist()
            .post("/authorize_agents")
            .query(true)
            .reply(201, nock_response)

        const expectedActions = [
            {type: APP_ACTIONS.SUBMIT_FORM},
            {type: APP_ACTIONS.ADD_AUTHORIZE_AGENTS,payload:  nock_response},
            {type: APP_ACTIONS.NOTSUBMIT_FORM},
            {payload: {args: ["/workbench/qrcode"], method: "push"}, type: "@@router/CALL_HISTORY_METHOD"}
        ]
        const store = mockStore({
                authorize_agent: {},
                user: {token: '12132324'}
            }
        );
        // expect.assertions(1);
        return store.dispatch(Actions.applyAuthorizeAgents({}))
            .then(() => {
                expect(store.getActions()).toEqual(expectedActions);
            })
    })
})