var request = require('superagent');

const AuthorizeAgentsAPI = {
  create: (params,formData) => {
    return request
      .post(`${process.env.REACT_APP_AMS_API_BASE}/authorize_agents`)
      .query(params)
      .set('Accept', 'application/json')
      .send(formData);
  }
}
export default AuthorizeAgentsAPI;
