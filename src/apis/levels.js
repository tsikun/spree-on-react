var request = require('superagent');
import SpreeAPITaxonAdapter from './ams-adapters/spree-api-taxon-adapter';

const LevelAPI = {
  getList: () => {
    return request
      .get(`${ process.env.REACT_APP_AMS_API_BASE }/levels`)
      .set('Accept', 'application/json')
      .then((response) => {
        return response;
      });
  }
};

export default LevelAPI;
