import APP_DEFAULTS from "../constants/app-defaults";

var request = require('superagent');
import SpreeAPIOrderAdapter from './ams-adapters/spree-api-order-adapter';
import SpreeAPIProductAdapter from "./ams-adapters/spree-api-product-adapter";

const TopUpAPI = {
  create: (tokenParam, formData={}) => {
    return request
      .post(`${process.env.REACT_APP_AMS_API_BASE}/top_ups`)
      .query(tokenParam)
        .field('top_up[amount]',formData.amount)
        .attach('top_up[proof_image_attributes][attachment]',formData.proof_image.nativeFile)
      .set('Accept', 'application/json');
  },

  update: (orderNumber, tokenParam, formData = {}) => {
    return request
      .put(`${process.env.REACT_APP_AMS_API_BASE}/checkouts/${orderNumber}`)
      .query(tokenParam)
      .set('Accept', 'application/json')
      .send(formData)
      .then((response) => {
        let processedResponse = SpreeAPIOrderAdapter.processItem(response.body);
        response.body = processedResponse;

        return response;
      });
  },
    getList: (tokenParam,params = {}) => {
        let apiBase = process.env.REACT_APP_AMS_API_BASE;
        let sanitizedQueryParams = {};

        sanitizedQueryParams.page = params.page_no || 1;
        sanitizedQueryParams.per_page = APP_DEFAULTS.perPage;
        sanitizedQueryParams.status = params.status;


        return request
            .get(`${ apiBase }/top_ups`)
            .query(tokenParam)
            .query(sanitizedQueryParams)
            .set('Accept', 'application/json')
            .send();
    }
}

export default TopUpAPI;
