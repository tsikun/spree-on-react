import React from 'react';
import {Route} from 'react-router-dom';

import HomePageConnector from './containers/home-page-connector';
import ProductShowConnector from './containers/product/product-show-connector';
import CartShowConnector from './containers/cart/cart-show-connector';

import AddressFormConnector from './containers/checkout-steps/address-form-connector';
import DeliveryFormConnector from './containers/checkout-steps/delivery-form-connector';
import PaymentFormConnector from './containers/checkout-steps/payment-form-connector';
import CheckoutConfirmationConnector from './containers/checkout-steps/confirmation-form-connector';
import CheckoutSuccessConnector from './containers/checkout-steps/checkout-success-connector';
import OrderListConnector from './containers/order/list-connector';
import OrderShowConnector from './containers/order/show-connector';

import FinanceTopUpConnector from './containers/finance/top_up'

import MessagePageConnector from './containers/message-page-connector'
import MessageTopUps from './components/message/top_ups'
import TeamPageConnector from './containers/team-page-connector'
import MinePageConnector from './containers/mine-page-connector'
import WorkbenchConnector from './containers/workbench-page-connector'
import AuthorizeConnector from './containers/workbench/authorize-connector'
import UpgradeConnector from './containers/workbench/upgrade-connector'
import CertificateConnector from './containers/workbench/certificate-connector'
import QRCodePage from './components/workbench/qr_code_page'
import ShopPageConnector from './containers/shop-page-connector'
import TopUpHistoryConnector from './containers/finance/top_up_history_connector'
import APP_ROUTES from './constants/app-routes';


export default function configRoutes() {
    return (
        <Route>
            <div>
                <Route exact path={APP_ROUTES.homePageRoute} component={HomePageConnector}/>
                <Route exact path={APP_ROUTES.searchPageRoute} component={HomePageConnector}/>
                <Route exact path='/products/:productId' component={ProductShowConnector}/>
                <Route exact path={APP_ROUTES.cartPageRoute} component={CartShowConnector}/>


                <Route exact path={APP_ROUTES.finance.topUpRoute} component={FinanceTopUpConnector}/>
                <Route exact path={APP_ROUTES.finance.topUpHistoryRoute} component={TopUpHistoryConnector} />

                <Route exact path={APP_ROUTES.messagePageRoute} component={MessagePageConnector}/>
                <Route exact path={APP_ROUTES.message.top_ups} component={MessageTopUps}/>
                <Route exact path={APP_ROUTES.teamPageRoute} component={TeamPageConnector}/>
                {/*Workbench routes start*/}
                <Route exact path={APP_ROUTES.workbenchPageRoute} component={WorkbenchConnector}/>
                <Route exact path={APP_ROUTES.workbench.authorizeRoute} component={AuthorizeConnector}/>
                <Route exact path={APP_ROUTES.workbench.upgradeRoute} component={UpgradeConnector}/>
                <Route exact path={APP_ROUTES.workbench.certificateRoute} component={CertificateConnector}/>
                <Route exact path={APP_ROUTES.workbench.qrCodePageRoute} component={QRCodePage}/>
                {/*Workbench routes end*/}

                <Route exact path={APP_ROUTES.minePageRoute} component={MinePageConnector}/>

                {/* shop routes begins*/}
                <Route exact path={APP_ROUTES.shop.homeRoute} component={ShopPageConnector}/>

                {/* shop routes ends*/}
                <Route exact path={APP_ROUTES.checkout.addressPageRoute} component={AddressFormConnector}/>
                <Route exact path={APP_ROUTES.checkout.deliveryPageRoute} component={DeliveryFormConnector}/>
                <Route exact path={APP_ROUTES.checkout.paymentPageRoute} component={PaymentFormConnector}/>
                <Route exact path={APP_ROUTES.checkout.confirmPageRoute} component={CheckoutConfirmationConnector}/>
                <Route exact path={APP_ROUTES.checkout.completePageRoute} component={CheckoutSuccessConnector}/>
                <Route exact path={APP_ROUTES.ordersPageRoute} component={OrderListConnector}/>
                <Route exact path='/orders/:orderId' component={OrderShowConnector}/>
                <Route exact path='/t/*' component={HomePageConnector}/>
            </div>
        </Route>
    );
};
