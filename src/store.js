import { createStore, applyMiddleware,compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import reduxReset from 'redux-reset';

import history from './browser-history';
import localStorageAPI from './services/local-storage-api';
import AppReducer from './reducers/index';
import APP_ACTIONS from './constants/app-actions'
/* Building a store */
const logger = createLogger();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let spreeStoreVariable;
const dataFromLocalStorage = localStorageAPI.load();

if (dataFromLocalStorage) {
  // spreeStoreVariable = createStore(AppReducer, {order: dataFromLocalStorage.order, user: dataFromLocalStorage.user, placedOrder: dataFromLocalStorage.placedOrder, locale: dataFromLocalStorage.locale}, applyMiddleware(thunk, routerMiddleware(history), logger),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()&& reduxReset());
  spreeStoreVariable = createStore(AppReducer, {order: dataFromLocalStorage.order, user: dataFromLocalStorage.user, placedOrder: dataFromLocalStorage.placedOrder, locale: dataFromLocalStorage.locale}, composeEnhancers(applyMiddleware(thunk, routerMiddleware(history), logger)), reduxReset());
}
else {
  // spreeStoreVariable = createStore(AppReducer, applyMiddleware(thunk, routerMiddleware(history), logger), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() &&reduxReset());
  spreeStoreVariable = createStore(AppReducer, composeEnhancers(applyMiddleware(thunk, routerMiddleware(history), logger)), reduxReset());
}

const spreeStore = spreeStoreVariable;
const user_initial={
    nickname: 'La Super',
    avator: "https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike116%2C5%2C5%2C116%2C38/sign=a83be289a9c27d1eb12b33967abcc60b/d043ad4bd11373f0fe063fb2a90f4bfbfbed04e5.jpg",
    realname: '刘强东',
    token: '501cc0b44c11d2ec0dc4fabce89f3ff83284d3efa2811ea7',
    parent: {
        nickname: 'super super',
        avator: "https://gss0.bdstatic.com/-4o3dSag_xI4khGkpoWK1HF6hhy/baike/c0%3Dbaike116%2C5%2C5%2C116%2C38/sign=a83be289a9c27d1eb12b33967abcc60b/d043ad4bd11373f0fe063fb2a90f4bfbfbed04e5.jpg",
        realname: '强爸',
    }
}
spreeStore.dispatch({type: APP_ACTIONS.LOGIN,payload: user_initial});
export default spreeStore;