import APP_ACTIONS from '../constants/app-actions';

const initialState = {levels: []};

const levelList = function(state = initialState, action) {
  switch (action.type) {
    case APP_ACTIONS.ADD_LEVELS:
      return Object.assign( {},state, action.payload);
    default:
      return state;
  }
}

export default levelList;
