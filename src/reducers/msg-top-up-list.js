import APP_ACTIONS from '../constants/app-actions';
import ProductModel from '../services/product-model';

const initialState = {
  msg_top_ups: [],
  meta: {}
};

const msgTopUpList = function(state = initialState, action) {
  let newProductList;
  let productInList;
  let oldProductList;

  switch (action.type) {
    case APP_ACTIONS.ADD_MSG_TOP_UPS:
      return Object.assign({}, state, action.payload);

    case APP_ACTIONS.APPEND_MSG_TOP_UPS:
      newProductList = action.payload.products.map((product) => { return product.id });
      oldProductList = state.products.filter ((product) => {
        return newProductList.indexOf(product.id) === -1;
      });

      return ( Object.assign (
              {},
              action.payload,
              { products: [...oldProductList, ...action.payload.products] }
      ));

    case APP_ACTIONS.ADD_MSG_TOP_UP:
      productInList = ProductModel.find(action.payload.id, state.products);

      if (productInList) {
        return state;
      }
      else {
        newProductList = Object.assign( [], state.products );
        newProductList.push(action.payload);
        return Object.assign ( {}, state, { products: newProductList } );
      }

    default:
      return state;
  }
}

export default msgTopUpList;
