import APP_ACTIONS from '../constants/app-actions';

const initialState = false;

const submitForm = function(state = initialState, action) {
  switch (action.type) {
    case APP_ACTIONS.SUBMIT_FORM:
      return true;
    case APP_ACTIONS.NOTSUBMIT_FORM:
      return false;
    default:
      return state;
  }
}

export default submitForm;
