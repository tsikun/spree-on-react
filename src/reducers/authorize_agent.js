import APP_ACTIONS from '../constants/app-actions';

const initialState = {
  number: null,
  wechat_ticket: null,
};

const authorize_agent = function(state = initialState, action) {

  switch (action.type) {
    case APP_ACTIONS.ADD_AUTHORIZE_AGENTS:
        console.log(action.payload);
        return Object.assign({}, state, action.payload);
      default:
      return state;
  }
}

export default authorize_agent;
