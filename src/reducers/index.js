import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';

import displayLoader from './display-loader';
import productList from './product-list';
import taxons from './taxons';
import order from './order';
import orderList from './order-list';
import flash from './flash';
import countryList from './country-list';
import currentCheckoutStep from './current-checkout-step';
import placedOrder from './placed-order';
import user from './user';
import locale from './locale';
import levelList from './level-list';
import submitForm from './submit-form'
import authorize_agent from './authorize_agent'
import msgTopUpList from './msg-top-up-list'

const AppReducer = combineReducers({
    displayLoader,
    submitForm,
    productList,
    taxons,
    order,
    orderList,
    flash,
    countryList,
    currentCheckoutStep,
    placedOrder,
    user,
    locale,
    levelList,
    authorize_agent,
    msgTopUpList,
    routing: routerReducer,
    form: formReducer
});

export default AppReducer;
