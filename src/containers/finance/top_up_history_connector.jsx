import React from 'react'
import {NavBarItem, Tab} from "react-weui";
import MessageTopUpItem from "../../components/message/top_up_item"
import {connect} from 'react-redux'
import TaxonAPI from "../../apis/taxons";
import {push} from "react-router-redux";
import Actions from "../../actions";
import APP_ROUTES from "../../constants/app-routes";
import TopUpHistory from "../../components/finance/top_up_history";
const mapStateToProps = (state, ownProps) => {
    return {
        taxons: state.taxons,
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTaxons: (taxons) => {
            if (taxons.length === 0) {
                dispatch (Actions.displayLoader());

                TaxonAPI.getList().then((response) => {
                    dispatch (Actions.addTaxons(response.body.taxons));
                    dispatch (Actions.hideLoader());
                });
            }
        },

        goToUserOrders: () => {
            dispatch (push(APP_ROUTES.ordersPageRoute));
        },

        logout: () => {
            dispatch(Actions.logout());
            dispatch(push(APP_ROUTES.homePageRoute));
        }
    };
};
const TopUpHistoryConnector = connect(mapStateToProps,mapDispatchToProps)(TopUpHistory)

export default TopUpHistoryConnector;