import React from 'react'
import {
    Panel,
    Page,
    MediaBoxHeader,
    MediaBox,
    MediaBoxTitle,
    MediaBoxBody,
    PanelHeader,
    PanelBody,
    Label,
    FormCell,
    CellBody,
    CellHeader,
    Uploader,
    Button, Form, Input, Gallery, GalleryDelete
} from 'react-weui'
import {connect} from "react-redux";
import Actions from '../../actions'
import APP_ROUTES from "../../constants/app-routes";
import {Link} from "react-router-dom";

class FinanceTopUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: '',
            gallery: false,
            files: []
        };
        this.handleAmountChange = this.handleAmountChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleFileClick=this.handleFileClick.bind(this);
        this.fileInput = null;
        this.setFileInput = elem => {
            this.fileInput = elem;
        }
        // this.fileInput = React.createRef();
    }

    handleFormSubmit(event) {
        let formData={
            amount: this.state.amount,
            proof_image: this.state.files[0]
        }
        this.props.createTopUp(formData);
        event.preventDefault();
    }

    handleAmountChange(event) {
        this.setState({amount: event.target.value});
    }

    handleFileChange(file, event) {
        let newFiles = [...this.state.files, file];
        this.setState({
            files: newFiles
        });
        event.preventDefault();
    }

    handleFileClick(e, file, i) {
        console.log('file click', file, i)
        this.setState({
            gallery: {
                id: i
            }
        })
    }


    renderGallery() {
        if (!this.state.gallery) return false;

        let srcs = this.state.files.map(file => file.data)

        return (
            <Gallery
                src={srcs}
                show
                defaultIndex={this.state.gallery.id}
                onClick={e => {
                    //avoid click background item
                    e.preventDefault()
                    e.stopPropagation();
                    this.setState({gallery: false})
                }}
            >

                <GalleryDelete onClick={(e, id) => {
                    this.setState({
                        files: this.state.files.filter((e, i) => i != id),
                        gallery: this.state.files.length <= 1 ? true : false
                    })
                }}/>
            </Gallery>
        )
    }

    render() {
        let thumbnails = this.state.files.map(file => {return {url: file.data}})
        return (
            <Page transition={true} infiniteLoader={false} ptr={false}>
                {this.renderGallery()}
                <Panel>
                    <MediaBox type="appmsg">
                        <MediaBoxHeader><img src={this.props.user.avator}/></MediaBoxHeader>
                        <MediaBoxBody>
                            <MediaBoxTitle>
                                {this.props.user.realname}({this.props.user.nickname})
                                <Link to={APP_ROUTES.finance.topUpHistoryRoute}>
                                <Button type="default" size="small" style={{float: 'right'}}>充值记录</Button>
                                </Link>
                            </MediaBoxTitle>
                        </MediaBoxBody>
                    </MediaBox>
                </Panel>
                <Panel>
                    <PanelHeader>
                        您的上级
                    </PanelHeader>
                    <PanelBody>
                        <MediaBox type="text">
                            <MediaBoxTitle> {this.props.user.parent.realname}({this.props.user.parent.nickname})</MediaBoxTitle>
                        </MediaBox>
                    </PanelBody>
                </Panel>
                <Form>
                    <FormCell>
                        <CellHeader>
                            <Label>金额</Label>
                        </CellHeader>
                        <CellBody>
                            <Input type="tel" placeholder="请输入金额" value={this.state.amount}
                                   onChange={this.handleAmountChange}/>
                        </CellBody>
                    </FormCell>
                </Form>
                <Form>
                    <FormCell>
                        <CellBody>
                            <Uploader maxCount={1} title="上传凭据" onChange={this.handleFileChange}
                                            files={thumbnails} onFileClick={this.handleFileClick} onError={msg => alert(msg)}/>
                        </CellBody>
                    </FormCell>
                </Form>
                <br/>
                <Button className='short_button' disabled={this.props.submitForm} onClick={this.handleFormSubmit}>确认</Button>
            </Page>

        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        submitForm: state.submitForm
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        createTopUp: (formData)=>
            dispatch(Actions.createTopUp(formData))
    };
};

const FinanceTopUpConnector = connect(mapStateToProps, mapDispatchToProps)(FinanceTopUp);
export default FinanceTopUpConnector;