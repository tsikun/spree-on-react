import { connect } from 'react-redux';

import WorkbenchPage from '../components/workbench-page';

const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

const WorkbenchPageConnector = connect(mapStateToProps, mapDispatchToProps)(WorkbenchPage);

export default WorkbenchPageConnector;
