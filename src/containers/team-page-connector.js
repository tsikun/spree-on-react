import { connect } from 'react-redux';

import TeamPage from '../components/team-page';



const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};


const TeamPageConnector = connect(mapStateToProps, mapDispatchToProps)(TeamPage);

export default TeamPageConnector;
