import { connect } from 'react-redux';

import MinePage from '../components/mine-page';

const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

const MinePageConnector = connect(mapStateToProps, mapDispatchToProps)(MinePage);

export default MinePageConnector;
