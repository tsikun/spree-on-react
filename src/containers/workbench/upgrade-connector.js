import React from 'react'
import { connect } from 'react-redux';
import {Select,Panel,Form,FormCell,CellHeader,Label,Cells,CellBody,Input,CellFooter,Cell,Button} from 'react-weui'

function Authorize(props) {
  return (
      <Panel className='form_page'>
          <img style={{backgroundColor: 'green'}}/>
          <h1>微商九大星王系统全球运营中心</h1>
          <Cells>
              <Cell>
                  <CellBody><Label>姓名</Label></CellBody>
                  <CellFooter>
                      <Label>Description</Label>
                  </CellFooter>
              </Cell>
              <Cell>
                  <CellBody><Label>当前等级</Label></CellBody>
                  <CellFooter>
                      <Label>级别</Label>
                  </CellFooter>
              </Cell>
              <FormCell select selectPos="after">
                  <CellBody>
                      <Label>申请级别</Label>
                  </CellBody>
                  <CellFooter>
                      <Label><Select data={[
                          {
                              value: 1,
                              label: '全国总代'
                          },
                          {
                              value: 2,
                              label: '区域代理'
                          },
                          {
                              value: 3,
                              label: '小代'
                          }
                      ]} /></Label>

                  </CellFooter>
              </FormCell>
              <Cell/>
          </Cells>

          <Button className='short_button'>确定</Button>
      </Panel>

  )
}

const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

const UpgradeConncector = connect(mapStateToProps, mapDispatchToProps)(Authorize);
export default UpgradeConncector;
