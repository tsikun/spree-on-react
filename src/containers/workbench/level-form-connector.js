import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import Actions from '../../actions';
import LevelForm from '../../components/workbench/level-form';
import LevelAPI from '../../apis/levels';

const mapStateToProps = (state, ownProps) => {
  return {
    levels: state.levelList.levels
  };
};

const mapDispatchToProps = (dispatch) => {
  return {

    handleAddressFormSubmit: (formData, order) => {
      return dispatch (Actions.goToNextStep(order, formData));
    },

    fetchLevels: () => {
      // dispatch (Actions.displayLoader());

      LevelAPI.getList().then((response) => {
        dispatch (Actions.addLevels(response.body));
      },
      (error) => {
        dispatch (Actions.showFlash('Unable to connect to server... Please try again later.'));
      })
    }

  };
};

const LevelFormConnector = connect(mapStateToProps, mapDispatchToProps)(LevelForm);

export default LevelFormConnector;
