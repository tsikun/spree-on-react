import { connect } from 'react-redux';

import MessagePage from '../components/message-page';

const mapStateToProps = (state, ownProps) => {
  return {

  };
};

const mapDispatchToProps = (dispatch) => {
  return {

  };
};

const MessagePageConnector = connect(mapStateToProps, mapDispatchToProps)(MessagePage);

export default MessagePageConnector;
