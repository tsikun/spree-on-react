import React from 'react';
import ReactDOM from 'react-dom';
// import { addLocaleData } from 'react-intl';

/*
  Load locales that need to be supported
*/
// import en from 'react-intl/locale-data/en';
// import es from 'react-intl/locale-data/es';

/*
  We are still loading bootstrap via CSS directly. Not using css-modules here.
*/
// import styles from 'bootstrap/dist/css/bootstrap.css';
// import bootstrapTheme from 'bootstrap/dist/css/bootstrap-theme.css';
import './components/styles/index.css';
import 'weui';
import 'react-weui/build/packages/react-weui.css';

import Main from './components/main';
import MessagePage from  './components/message-page'
/*
  These are non css-modules styles.
*/


// addLocaleData([...en, ...es]);

ReactDOM.render(
  <Main />
  ,
  document.getElementById('container')
);


