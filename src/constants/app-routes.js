const APP_ROUTES = {
    homePageRoute: '/',
    searchPageRoute: '/search/:searchTerm',
    cartPageRoute: '/cart',
    ordersPageRoute: '/orders',
    workCenterPageRoute: '/workcenter',
    minePageRoute: '/mine',
    messagePageRoute: '/message',
    teamPageRoute: '/team',
    workbenchPageRoute: '/workbench',
    workbench: {
        authorizeRoute: '/workbench/authorize',
        upgradeRoute: '/workbench/upgrade',
        certificateRoute: '/workbench/certificate',
        qrCodePageRoute: '/workbench/qrcode'
    },
    finance: {
        topUpRoute: '/finance/top_up',
        topUpHistoryRoute: '/finance/top_up_history'
    },
    message: {
        top_ups: '/message/top_ups'
    },
    shop: {
        homeRoute: '/shop',
        detailRoute: '/shop/detail'
    },
    checkout: {
        addressPageRoute: '/checkout/address',
        editAddressPage: '/checkout/edit_address',
        deliveryPageRoute: '/checkout/delivery',
        paymentPageRoute: '/checkout/payment',
        confirmPageRoute: '/checkout/confirm',
        completePageRoute: '/checkout/complete'
    }
};

export default APP_ROUTES;
