import APP_ROUTES from './app-routes'
export const AGENT_GROUP = [
  {
    title: '授权审核',
    icon: 'icon-dailishang1',
    msg_count: 5,
    url: ''
  },
  {
    title: '下级请求申请',
    icon: 'icon-xiajiangyiji',
    msg_count: 8,
    url: ''
  },
  {
    title: '升级申请',
    icon: 'icon-shengji2',
    msg_count: 8,
    url: ''
  },
  {
    title: '降级申请',
    icon: 'icon-jiangji1',
    msg_count: 8,
    url: ''
  }
];
export const MONEY_GROUP = [
  {
    title: '扣款申请',
    icon: 'icon-zidongkoukuan1',
    msg_count: 5,
    url: ''
  },
  {
    title: '打款申请',
    icon: 'icon-bank-card',
    msg_count: 8,
    url: ''
  },
  {
    title: '账单充值申请',
    icon: 'icon-kuaisudakuan',
    msg_count: 8,
    url: ''
  }
];
export const WORKBENCH_MENU={
  MORE_MENU: [
      {
          title: '我要充值',
          icon: 'icon-kongbaiqiachongzhi',
          color: 'rgb(225, 158, 40)',
          url: ''
      },
      {
          title: '云仓下单',
          icon: 'icon-xiazai',
          color: 'rgb(80, 94, 255)',
          url: ''
      },
      {
          title: '考试中心',
          icon: 'icon-kaoshi',
          color: 'rgb(230, 67, 64)',
          url: ''
      }
  ],
    AUTHORIZE_MENU: [
        {
            title: '我要授权',
            icon: 'icon-shouquan',
            color: 'rgb(254, 92, 111)',
            url: APP_ROUTES.workbench.authorizeRoute
        },
        {
            title: '我要升级',
            icon: 'icon-ji2',
            color: 'rgb(40, 138, 225)',
            url: APP_ROUTES.workbench.upgradeRoute
        },
        {
            title: '我的证书',
            icon: 'icon-zhengshu',
            color: 'rgb(47, 220, 120)',
            url: ''
        },
        {
            title: '我的助理',
            icon: 'icon-zhuli',
            color: 'rgb(65, 187, 250)',
            url: ''
        }
    ],
    MONEY_MENU: [
        {
            title: '我要充值',
            icon: 'icon-kongbaiqiachongzhi',
            color: 'rgb(225, 158, 40)',
            url: ''
        },
        {
            title: '下级扣款',
            icon: 'icon-iconfont1336tuikuandaituikuan',
            color: 'rgb(23, 180, 45)',
            url: ''
        },
        {
            title: '下级充值',
            icon: 'icon-proxyCzhi',
            color: 'rgb(230, 131, 255)',
            url: ''
        },
        {
            title: '财务报表',
            icon: 'icon-report1',
            color: 'rgb(230, 67, 64)',
            url: ''
        }
    ],
    ORDER_MENU: [
        {
            title: '我要下单',
            icon: 'icon-iconfontcart',
            color: 'rgb(64, 169, 230)',
            url: ''
        },
        {
            title: '我要发货',
            icon: 'icon-daifahuo',
            color: 'rgb(230, 67, 64)',
            url: ''
        },
        {
            title: '云仓下单',
            icon: 'icon-xiazai',
            color: 'rgb(80, 94, 255)',
            url: ''
        }
    ],
    OTHER_MENU: [
        {
            title: '素材中心',
            icon: 'icon-act',
            color: 'rgb(162, 208, 17)',
            url: ''
        },
        {
            title: '微培训',
            icon: 'icon-peixun1',
            color: 'rgb(75, 202, 255)',
            url: ''
        }
    ]

}

export const MINE_GROUP=[
    {
        title: '库存',
        img: 'iocn_stock.png',
        url: ''
    },
    {
        title: '活动订单统计',
        img: 'icon_personal5.png',
        url: ''
    },
    {
        title: '收货地址',
        img: 'icon_address.png',
        msg_count: 8,
        url: ''
    },
    {
        title: '城市合伙人',
        img: 'iocn_tie.png',
        msg_count: 5,
        url: ''
    },
    {
        title: '账单',
        img: 'iocn_bill.png',
        msg_count: 8,
        url: ''
    },
    {
        title: '微培训',
        img: 'iocn_train.png',
        msg_count: 8,
        url: ''
    },
    {
        title: '我的上级',
        img: 'iocn_up.png',
        msg_count: 0,
        url: ''
    },
    {
        title: '设置',
        img: 'iocn_setup.png',
        msg_count: 8,
        url: ''
    }

];
export const FOOTER_GROUP=[
    {
        title: '消息',
        url: '/message',
    },
    {
        title: '团队',
        url: '/team',
    },
    {
        title: '工作台',
        url: '/workbench',
    },
    {
        title: '订单',
        url: '/shop',
    },
    {
        title: '我的',
        url: '/mine',
    }
]
