import React from 'react';
import {Cell,CellHeader,CellBody,CellFooter,Badge} from 'react-weui';
function MenuItem(props) {
    const cell_hd=props.icon ?
        <i className={`icon iconfont ${props.icon}`}/>:
        <img className="icon_img" src={require(`../images/${props.img}`)}/>

    const cell_ft=props.msg_count?  <Badge>{props.msg_count}</Badge> : null

    return (
        <Cell href={props.url} access>
            <CellHeader>
                {cell_hd}
            </CellHeader>
            <CellBody>
                {props.title}
            </CellBody>
            <CellFooter>{cell_ft}</CellFooter>
        </Cell>
    );
}

export default MenuItem;
