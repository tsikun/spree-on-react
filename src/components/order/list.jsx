import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

import Layout from '../layout';
import OrderPanelView from './panel-view';
import Loader from '../shared/loader';
import {Tab,NavBarItem} from 'react-weui'
class OrderList extends Component {
  constructor(props){
    super(props);

    this.state = {
      displayLoader: true
    };
  };

  componentDidMount() {
    if (this.props.user.id) {
      this.props.loadOrders(this.props.user.token).then((response) => {
        this.setState({ displayLoader: false });
      });
    }
    else {
      this.props.handleUserNotLoggedIn();
    }
  };

  render() {
    let orderListMarkup = this.props.orders.map((order, idx) => {
      return (<OrderPanelView order={ order } key={ idx } />);
    });

    return (
        <Tab type="navbar">
          <NavBarItem label="待审核">

          </NavBarItem>
          <NavBarItem label="已审核">

          </NavBarItem>
        </Tab>
    );
  };
};

export default OrderList;
