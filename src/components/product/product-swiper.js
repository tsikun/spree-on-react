import React from 'react';
import  Swiper from 'react-id-swiper/lib/custom'

export default function ProductSwiper(props) {
    const params = {
        containerClass: 'swiper-carousel-container'
    }
    const images=props.images? props.images.map((i)=> <img src={i}/> ): null;
    return (
        <Swiper {...params}>
            {images}
        </Swiper>
    );
};