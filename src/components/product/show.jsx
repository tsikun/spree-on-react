import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {FormattedMessage} from 'react-intl';

import ProductProperties from './properties';
import ImageViewer from './image-viewer';
import VariantsList from './variants-list';
import Layout from '../layout';
import APP_ROUTES from '../../constants/app-routes';
import ProductModel from '../../services/product-model';
import Loader from '../shared/loader';
import {Page,Panel, Cells, Cell, CellBody} from 'react-weui';
import WorkbenchSwiper from '../shared/workbench-swiper'
import ProductSwiper from './product-swiper'

class ProductShow extends Component {
    constructor(props) {
        super(props);
        this.onChangeVariant = this.onChangeVariant.bind(this)
        this.state = {
            currentProduct: {},
            productId: '',
            currentVariant: null
        }
    };

    onChangeVariant(variant) {
        this.setState({currentVariant: variant})
    }

    componentDidMount() {
        let productId = this.props.match.params.productId;
        let product = ProductModel.findBySlug(productId, this.props.products);

        /* Set productId in internal state */
        this.setState({
            productId: productId
        });

        if (product) {
            this.setState({
                currentProduct: product,
                currentVariant: (product.variants[0] || product.master)
            });
        }
        else {
            this.props.fetchProductFromAPI(productId);
        }
    };

    componentWillReceiveProps(nextProps) {
        let productId = this.state.productId;
        let product = ProductModel.findBySlug(productId, nextProps.products);

        if (product) {
            this.setState({
                currentProduct: product,
                currentVariant: (product.variants[0] || product.master)
            });
        }
        else {
            this.props.fetchProductFromAPI(productId);
        }
    };

    addProductToCart() {
        this.props.addProductToCart(this.state.currentVariant.id, 1);
    };

    render() {
        let renderString = null;
        let variantListNode = null;
        let {currentVariant} = this.state;

        if (currentVariant) {
            let isVariantInCart = this.props.lineItems.find((lineItem) => {
                return (lineItem.variant_id === currentVariant.id)
            })
            let addToCartButtonNode =<a href="#" onClick={this.addProductToCart.bind(this)} className="table-cell vm bg-orange fs15 colf open-popup" >
                    <i className="ico-foot2"></i>加入购物车
                </a>;

            if (this.state.currentProduct.variants.length > 1) {
                variantListNode = <div className="product-option-row variant-block">
                    <div className="varient-title">Select Variant</div>
                    <VariantsList currentVariant={ currentVariant }
                                  variantsList={ this.state.currentProduct.variants }
                                  onChangeVariant={ this.onChangeVariant }/>
                </div>;
            }

            if (isVariantInCart) {

            }

            renderString =   <Page ptr={false} infiniteLoader={false}>
                <div>
                    <ProductSwiper images={false} />
                    <h5>{ currentVariant.name }  </h5>
                    <h6> ￥{ currentVariant.price }  </h6>
                </div>
                <Cells>
                    <Cell>
                        <CellBody>
                            产品说明
                        </CellBody>
                    </Cell>
                    <Cell>
                        <CellBody>
                            產頻說明
                        </CellBody></Cell>
                </Cells>
                <Cells>
                    <Cell>
                        <CellBody>
                            产品选项
                        </CellBody>
                    </Cell>
                </Cells>
                <Cells>
                    <Cell>
                        <CellBody>
                            产品详情
                        </CellBody>
                        { currentVariant.description }

                    </Cell>
                </Cells>
                <div className="bottom-fixed">
                    <footer className="table bt-f1 bgf tc fs10 footer">
                        <a href="#" className="table-cell vm m-col-14 br"><i className="ico-foot0 block ma"></i>首页</a>
                        <a href="#" className="table-cell vm m-col-14 br"><i className="ico-foot1 block ma hint-num"><em>2</em></i>购物车</a>
                        {addToCartButtonNode}
                        <a href="javascript:;" className="table-cell vm bg-red fs15 colf open-popup" data-target="#spec-pop">
                            <i className="ico-foot3"></i>立即购买</a>
                    </footer>
                </div>
            </Page>

        }
        return (
          renderString

        );
    };
}
;

export default ProductShow;
