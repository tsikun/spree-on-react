import React, {Component} from 'react';
import Layout from './layout';
import {AGENT_GROUP, MONEY_GROUP} from './../constants/app-menu';
import MenuList from './menu-list'
import ICON_NOTICE from '../images/iocn_notice.png'
import {
    CellsTitle,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    Panel,
    PanelBody,
} from 'react-weui';

class MessagePage extends Component {
    render() {
        return (
            <Layout>
                <Panel>
                    <PanelBody>
                        <MediaBox type="appmsg" href="javascript:void(0);">
                            <MediaBoxHeader><img src={ICON_NOTICE}/></MediaBoxHeader>
                            <MediaBoxBody>
                                <MediaBoxTitle>系统消息</MediaBoxTitle>
                                <MediaBoxDescription>
                                </MediaBoxDescription>
                            </MediaBoxBody>
                        </MediaBox>
                    </PanelBody>
                </Panel>

                <CellsTitle>代理信息</CellsTitle>
                <MenuList items={AGENT_GROUP}/>

                <CellsTitle>资金信息</CellsTitle>
                <MenuList items={MONEY_GROUP}/>
            </Layout>
        );
    };
}
;

export default MessagePage;
