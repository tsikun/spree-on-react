import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import URLSanitizer from '../services/url-sanitizer';

import styles from './styles/components/product-tile.scss';
import {Panel, PanelBody, PanelFooter} from "react-weui";

class ProductTile extends Component {
    constructor(props) {
        super(props);

    };
    render() {
        let image = this.props.product.master.images[0] || {};
        let productName = this.props.product.name;
        let productShowURL = '/products/' + this.props.product.slug;
        let imageUrl = image.product_url ? URLSanitizer.makeAbsolute(image.product_url) : null;
        return (
            <div className={" " + styles.productBlock}>
                <div className={"thumb-img-block " + styles.productImageBlock}>
                    <Link to={productShowURL} className={"product-link " + styles.productImageHolder}>
                        <img className={"product-tile-image " + styles.productMainImage}
                             alt={productName}
                             src={imageUrl}>
                        </img>
                    </Link>
                </div>
                <div className={" " + styles.productInfoBlock}>
                    <div title={productName}>
                        <Link to={productShowURL} className={" " + styles.productTitle}>
                            {productName}
                        </Link>
                    </div>
                    <span className={styles.productYuan}>￥</span><span
                    className={styles.productPrice}>{this.props.product.master.price}</span>
                </div>

            </div>
        );
    };
};

export default ProductTile;
