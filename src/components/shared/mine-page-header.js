import React from 'react';
import {MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription
} from 'react-weui';

class MinePageHeader extends React.Component {
    render(){
        return (
            <MediaBox type="appmsg" href="javascript:void(0);">
                <MediaBoxHeader></MediaBoxHeader>
                <MediaBoxBody>
                    <MediaBoxTitle>Media heading</MediaBoxTitle>
                    <MediaBoxDescription>
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                    </MediaBoxDescription>
                </MediaBoxBody>
            </MediaBox>
        );
    }
}
export default MinePageHeader;