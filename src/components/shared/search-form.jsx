import React from 'react';

import {
    //main component
    SearchBar,
    //for display data
    Panel,
    PanelHeader,
    PanelBody,
    PanelFooter,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    Cell,
    CellBody,
    CellFooter
} from 'react-weui';

const CellMore = () => (
    <Cell access link>
        <CellBody>More</CellBody>
        <CellFooter />
    </Cell>
)

export default class SearchForm extends React.Component {
    state={
        searchText: '',
        results: []
    };

    handleChange(text, e){
        let keywords = [text];
        let results =[]; //SampleData.filter(/./.test.bind(new RegExp(keywords.join('|'),'i')));

        if(results.length > 3) results = results.slice(0,3);
        this.setState({
            results,
            searchText:text,
        });
    }

    render() {
        return (
            <div>
                <SearchBar
                    onChange={this.handleChange.bind(this)}
                    defaultValue={this.state.searchText}
                    placeholder="搜索团队成员"
                    lang={{
                        cancel: '取消'
                    }}
                />

                <Panel style={{display: this.state.searchText ? null: 'none', marginTop: 0}}>
                    <PanelBody>
                        {
                            this.state.results.length > 0 ?
                                this.state.results.map((item,i)=>{
                                    return (
                                        <MediaBox key={i} type="appmsg" href="javascript:void(0);">
                                            <MediaBoxHeader></MediaBoxHeader>
                                            <MediaBoxBody>
                                                <MediaBoxTitle>{item}</MediaBoxTitle>
                                                <MediaBoxDescription>
                                                    You may like this name.
                                                </MediaBoxDescription>
                                            </MediaBoxBody>
                                        </MediaBox>
                                    )
                                })
                                : <MediaBox>没有相关搜索结果</MediaBox>
                        }
                    </PanelBody>
                    <PanelFooter href="javascript:void(0);">
                        <CellMore />
                    </PanelFooter>
                </Panel>
            </div>
        );
    }
};
