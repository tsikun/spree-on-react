import React from 'react';
import {Link} from 'react-router-dom';
function TabMenu(props) {
    const items=props.items;
    const itemList=items.map((i,index)=>
        (<li key={index}>
            <Link to={i.url}><p><i className={`icon iconfont ${i.icon}`} style={{color: i.color}}></i></p>
            <p>{i.title}</p>
            </Link>
        </li>)
    )
    return (
        <div className="wx_desk_menu">
            <ul>
                {itemList}
            </ul>
        </div>
    )
}
export default TabMenu;