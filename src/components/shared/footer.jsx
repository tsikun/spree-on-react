// import React, { Component } from 'react';
// import LocaleSelectorConnector from '../../containers/locale-selector-connector';
//
// class Footer extends Component {
//   render() {
//     return (
//       <footer className="footer-section">
//         <div className="container">
//           <div className="row">
//             <div className="col-sm-6 footer-left-content">
//               &copy; 2017 &nbsp;
//               <a href="http://vinsol.com" target="_blank">
//                 VinSol
//               </a>
//             </div>
//             <div className="col-sm-6 footer-right-content">
//               <LocaleSelectorConnector />
//             </div>
//           </div>
//         </div>
//       </footer>
//     );
//   }
// }
//
// export default Footer;

import React from 'react';

import {
    TabBar,
    TabBarItem,
    TabBarIcon,
    TabBarLabel
} from 'react-weui';
import { NavLink } from 'react-router-dom'
import {FOOTER_GROUP} from "../../constants/app-menu";


export default class Footer extends React.Component {
    render() {
        const tab_items=FOOTER_GROUP.map((i,indx)=>(
            <TabBarItem key={indx}>
                <NavLink to={i.url} activeClassName="tab-item_on" className='tab-item_off'>
                    <TabBarIcon>
                        <img className='img_off' src={require(`../../images/nav${indx+1}.png`)}/>
                        <img className='img_on' src={require(`../../images/nav${indx+1}_on.png`)}/>
                    </TabBarIcon>
                    <TabBarLabel>{i.title}</TabBarLabel>
                </NavLink>
            </TabBarItem>
        ));
        return (
                <TabBar>
                    {tab_items}
                </TabBar>
        );
    }
};
