import React from 'react';
import  Swiper from 'react-id-swiper/lib/custom'

class WorkbenchMsgSwiper extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        const params = {
            autoplay:{
                delay: 4000,
            },
            direction: 'vertical',
            loop:true,
            containerClass: 'swiper-msg-container'

        }
        return (
            <Swiper {...params} >
                <div>Slide 1</div>
                <div>Slide 2</div>
                <div>Slide 3</div>
                <div>Slide 4</div>
                <div>Slide 5</div>
            </Swiper>
        );
    }
}
export default WorkbenchMsgSwiper;