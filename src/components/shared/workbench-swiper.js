import React from 'react';
import  Swiper from 'react-id-swiper/lib/custom'

class WorkbenchSwiper extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        const params = {
            slidesPerView: 1,
            autoplay:{
                delay: 4000,
            },
            loop:true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            containerClass: 'swiper-carousel-container'
        }
        return (
            <Swiper {...params}>
                <div>Slide 1</div>
                <div>Slide 2</div>
                <div>Slide 3</div>
                <div>Slide 4</div>
                <div>Slide 5</div>
            </Swiper>
        );
    }
}
export default WorkbenchSwiper;