import React from 'react';
import {Grids,Grid,GridIcon,GridLabel} from 'react-weui';
class MoneyGrids extends React.Component {
    render(){
        return (
            <Grids className='mine_header'>
                <Grid>
                    <GridIcon>
                        5888880.00
                        <span>元</span>
                    </GridIcon>
                    <GridLabel>总业绩</GridLabel>
                </Grid>
                <Grid>
                    <GridIcon>0.00<span>元</span></GridIcon>

                    <GridLabel>本月业绩</GridLabel>
                </Grid>
                <Grid>
                    <GridIcon>0.00<span>元</span></GridIcon>

                    <GridLabel>账户余额</GridLabel>
                </Grid>
            </Grids>
        );
    }
}
export default MoneyGrids;