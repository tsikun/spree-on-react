import React from 'react'
import {Cells,Cell,CellBody} from 'react-weui'
function CategoryMenu(props) {
    return (
        <aside>
            <div className="menu-left scrollbar-none" id="sidebar">
                <Cells>
                    <Cell>
                        <CellBody>
                            Title
                        </CellBody>
                    </Cell>
                    <Cell>
                        <CellBody>
                            Title
                        </CellBody>
                    </Cell>
                    <Cell>
                        <CellBody>
                            Title
                        </CellBody>
                    </Cell>
                </Cells>
            </div>
        </aside>
    )
}
export default CategoryMenu;