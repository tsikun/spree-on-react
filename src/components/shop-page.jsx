import React, {Component} from 'react';

import ProductList from './product-list';
// import Loader from './shared/loader';
import SearchForm from './shared/search-form'
// import CategoryMenu from './shared/category_menu'
import {Flex, FlexItem, Cells, Cell, CellBody, Page} from 'react-weui'
// import ProductTile from "./product-tile";

class ShopPage extends Component {

    componentDidMount() {
        console.log('ShopPage Loading');
        this.props.triggerInitialSetup(this.props.match.params['searchTerm']);
    };

    /* If home page icon is clicked. */
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.location.pathname !== this.props.location.pathname) {
            this.props.triggerInitialSetup(this.props.location.pathname);
        }
    }

    render() {
        return (

            <div className='shop_page'>
                {/*<div className='search-area'>*/}
                <SearchForm/>
                {/*</div>*/}
                <div className="list-container">
                    <div className="left-category">
                        <Cells style={{whiteSpace: 'nowrap', marginTop: '0'}}>
                            <Cell className='active-category'>全部分类</Cell>
                            <Cell>蜜柚系列</Cell>
                            <Cell>蜜柚系列</Cell>
                            <Cell>蜜柚系列</Cell>
                            <Cell>蜜柚系列</Cell>
                            <Cell>蜜柚系列</Cell>
                            <Cell>HNM</Cell>
                        </Cells>
                    </div>
                    <div className="right-product">
                        <ProductList products={ this.props.products }
                                     loadMoreProducts={ this.props.loadMoreProducts }
                                     pageCount={ this.props.pageCount } />
                        {/*<div id="category4" className="jd-category-loadfail" style={{display: 'none'}}>*/}
                            {/*<div className="loadfail-content">*/}
                                {/*<div className="failing"></div>*/}
                                {/*<span>加载失败</span><a href="javascript:void(0);" id="category5"*/}
                                                    {/*className="btn-loadfail"></a></div>*/}
                        {/*</div>*/}
                    </div>
                </div>
            </div>
        );
    };
};

export default ShopPage;
