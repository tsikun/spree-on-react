import React, {Component} from 'react';
import Layout from './layout';
import WorkbenchSwiper from './shared/workbench-swiper'
import WorkbenchMsgSwiper from './shared/workbench-msg-swiper'
import {WORKBENCH_MENU} from '../constants/app-menu'
import TabMenu from './shared/tab_menu'
import {
    MediaBox,
    Cell,
    CellHeader,
    CellBody,
    CellFooter,
    Panel,
    Tab,
    NavBarItem
} from 'react-weui';
import Headimg from '../images/132.png'
import IconDesk1 from '../images/icon_desk1.png'
import IconDesk2 from '../images/icon_desk2.png'
import IconDesk3 from '../images/icon_desk3.png'
import IconDesk4 from '../images/icon_desk4.png'
import IconDesk5 from '../images/icon_desk5.png'
import {Link} from "react-router-dom";
import APP_ROUTES from '../constants/app-routes'

class WorkbenchPage extends Component {
    render() {
        return (
            <Layout>
                <Panel className='workbench_upper'>
                    <MediaBox type='appmsg'>
                        <Cell className='desk-head-user'>
                            <CellHeader className='desk-user-img'><img src={Headimg}/></CellHeader>
                            <CellBody>
                                郑惠媚(La Super)
                            </CellBody>
                            <CellFooter><i className="icon iconfont icon-xiaoxi" ></i></CellFooter>
                        </Cell>
                    </MediaBox>
                    <Panel className='swiper_container'>
                    <WorkbenchSwiper/>
                    </Panel>
                    <div className='desk-head-msg'>
                        <i className="icon iconfont icon-laba"></i>
                    <WorkbenchMsgSwiper/>
                        <i className="icon iconfont icon-youjiantou-copy"></i>
                    </div>
                    <div className="desk_icon_contianer">
                        <ul className="desk_ul_1">
                            <li >
                                <Link to={APP_ROUTES.workbench.authorizeRoute} ><img src={IconDesk1}/></Link>
                            </li>
                            <li >
                                <img src={IconDesk2}/>
                            </li>
                            <li ><img src={IconDesk3}/></li>
                        </ul>
                        <ul className="desk_ul_2">
                            <li ><Link to={APP_ROUTES.workbench.upgradeRoute}><img src={IconDesk4}/></Link></li>
                            <li ><img src={IconDesk5}/></li>
                        </ul>
                    </div>
                    <div className='more_funct'>
                    更多你想要的功能
                    </div>
                    <div className='wx_head_menu'>
                    <TabMenu items={WORKBENCH_MENU.MORE_MENU}/>
                    </div>
                </Panel>

                <Tab type="navbar" className='workbench_lower'>
                    <NavBarItem label="授权管理">
                        <TabMenu items={WORKBENCH_MENU.AUTHORIZE_MENU}/>
                    </NavBarItem>
                    <NavBarItem label="财务管理">
                        <TabMenu items={WORKBENCH_MENU.MONEY_MENU}/>
                    </NavBarItem>
                    <NavBarItem label="订单管理">
                        <TabMenu items={WORKBENCH_MENU.ORDER_MENU}/>
                    </NavBarItem>
                    <NavBarItem label="其他应用">
                        <TabMenu items={WORKBENCH_MENU.OTHER_MENU}/>
                    </NavBarItem>
                </Tab>
            </Layout>
        );
    };
}

export default WorkbenchPage;
