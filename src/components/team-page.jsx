import React, {Component} from 'react';
import Layout from './layout';
import {AGENT_GROUP, MONEY_GROUP} from './../constants/app-menu';
import MenuList from './menu-list'
import SearchForm from './shared/search-form'

import {
    Cells,
    Cell,
    CellHeader,
    CellBody,
    CellFooter,
    CellsTitle,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    Panel,
    PanelBody,
    PanelHeader
} from 'react-weui';

class TeamPage extends Component {
    render() {
        return (
            <Layout>
                <SearchForm/>
                <Panel>
                    <PanelHeader>
                        <MediaBox type="appmsg" href="javascript:void(0);">
                            <MediaBoxHeader></MediaBoxHeader>
                            <MediaBoxBody>
                                <MediaBoxTitle>Media heading</MediaBoxTitle>
                                <MediaBoxDescription>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
                                </MediaBoxDescription>
                            </MediaBoxBody>
                        </MediaBox>
                    </PanelHeader>
                    <PanelBody>
                        <MediaBox type="small_appmsg">
                            <Cells>
                                <Cell href="javascript:;" access>
                                    <CellHeader></CellHeader>
                                    <CellBody>
                                        <p>Media heading</p>
                                    </CellBody>
                                    <CellFooter/>
                                </Cell>
                                <Cell href="javascript:;" access>
                                    <CellHeader></CellHeader>
                                    <CellBody>
                                        <p>Media heading</p>
                                    </CellBody>
                                    <CellFooter/>
                                </Cell>
                            </Cells>
                        </MediaBox>
                    </PanelBody>
                </Panel>

                <CellsTitle>代理信息</CellsTitle>
                <MenuList items={AGENT_GROUP}/>
            </Layout>
        );
    };
};

export default TeamPage;
