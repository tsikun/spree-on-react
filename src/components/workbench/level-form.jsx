import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector, SubmissionError } from 'redux-form';
import {Button, Cell, CellBody, CellFooter, Cells, Form, FormCell, Label, Select} from "react-weui";
import LevelAPI from '../../apis/levels'

class LevelForm extends Component {

  /* Render this step only if order is present and in a valid checkout state. */
  constructor(props) {
      super(props);
      this.state = {
          level_id: '1'
      }
      this.handleLevelFromSubmit=this.handleLevelFromSubmit.bind(this);
      this.handleChange=this.handleChange.bind(this);
  }
  componentWillMount() {

  };
  handleLevelFromSubmit(event){
      alert('Your favorite flavor is: ' + this.state.level_id);
      event.preventDefault();
  }
  handleChange(event) {
      this.setState({level_id: event.target.value});
  }
  componentDidMount () {
      if (this.props.levels.length === 0) {
          this.props.fetchLevels();
      }
  };

  render() {
      const levelOptions= this.props.levels.map((i,index)=>
          <option key={index} value={i.id} label={i.name}></option>
      );
      return (
        <form onSubmit={this.handleLevelFromSubmit}>
            <Form className='level_form' >
                <FormCell>
                    <CellBody><Label>姓名</Label></CellBody>
                    <CellFooter>
                        <Label>名称</Label>
                    </CellFooter>
                </FormCell>
                <Cell>
                    <CellBody>
                        <Label>当前等级</Label>
                    </CellBody>
                    <CellFooter>
                        <Label>级别</Label>
                    </CellFooter>
                </Cell>
                <FormCell select selectPos="after">
                    <CellBody>
                        <Label>选择层级</Label>
                    </CellBody>
                    <CellFooter>
                        <select className="weui-select" value={this.state.level_id} onChange={this.handleChange}>
                            { levelOptions}
                        </select>
                    </CellFooter>
                </FormCell>
                <Cell/>
            </Form>
            <Button  className='short_button' >生成链接</Button>
        </form>
    );
  };

  _sanitizedErrorMessages (errors) {

  }

  // def shipping_eq_billing_address?
  //     (bill_address.empty? && ship_address.empty?) || bill_address.same_as?(ship_address)
  //   end
};

LevelForm = reduxForm({
  form: 'levelForm'
})(LevelForm);

const selector = formValueSelector('levelForm');
LevelForm = connect(
  state => {
    return {

    };
  }
)(LevelForm)

export default LevelForm;
