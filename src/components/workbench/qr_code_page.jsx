import React, { Component } from 'react';
import {
    Cells,
    Cell,
    CellHeader,
    CellBody,
    CellFooter,
    CellsTitle,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    Panel,
    PanelBody,
    PanelHeader,
    Page
} from 'react-weui';

class QRCodePage extends Component {

    render() {

        return (
            <Page className='qr_code_page'>
                <Panel className='qr_code_panel'>
                    <PanelBody>
                        <MediaBox type="appmsg" href="javascript:void(0);">
                            <MediaBoxHeader><img src={require('../../images/132.png')}/>                  </MediaBoxHeader>
                            <MediaBoxBody>
                                <MediaBoxTitle>Media heading</MediaBoxTitle>
                                <MediaBoxDescription>
                                    Cras sit amet nibh libero, in grav
                                </MediaBoxDescription>
                            </MediaBoxBody>
                        </MediaBox>
                        <div className='qr_code_img'>
                        <img src={require('../../images/showqrcode.png')}/>
                        </div>
                        <div className="txt">
                            <p>1. 扫描或识别二维码，即可填写信息。</p>
                            <p>2. 邀请链接有效时间为 <span>30分钟</span>。</p>
                            <p>3. 过期后可重新生成链接。</p>
                        </div>

                    </PanelBody>

                </Panel>
            </Page>
        );
    };

};

export default QRCodePage;