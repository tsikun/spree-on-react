import React, {Component} from 'react';
import Layout from './layout';
import {MINE_GROUP} from './../constants/app-menu';
import MenuList from './menu-list'
import MoneyGrids from './shared/money-grids'
import MinePageHeader from './shared/mine-page-header'
import {
    Panel,
    PanelHeader,
    PanelBody
} from 'react-weui';

class MinePage extends Component {
    render() {
        return (
            <Layout>
                <Panel>
                    <PanelHeader>
                        <MinePageHeader/>
                    </PanelHeader>
                    <PanelBody>
                        <MoneyGrids/>
                    </PanelBody>
                </Panel>
                <MenuList items={MINE_GROUP}/>
            </Layout>
        );
    };
}

export default MinePage;
