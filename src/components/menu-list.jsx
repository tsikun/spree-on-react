import React from 'react';
import MenuItem from './menu-item'
import {Cells} from 'react-weui'
function MenuList(props) {
    const items=props.items;
    const listItems=items.map((i,index)=>
        <MenuItem key={index} title={i.title} icon={i.icon} msg_count={i.msg_count} url={i.url} img={i.img}/>
    );
    return (
        <Cells>{listItems}</Cells>
    )
}
export default MenuList;