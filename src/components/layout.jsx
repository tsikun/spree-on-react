import React, { Component } from 'react';
import HeaderConnector from '../containers/header-connector';
import FlashConnector from '../containers/flash-connector';
import {TabBody,Tab} from 'react-weui';
import Footer from './shared/footer';

class Layout extends Component {
  render() {

    return (
      <Tab >
          <TabBody> { this.props.children }
          </TabBody>
          <Footer />
      </Tab>
    );
  }
}

export default Layout;
