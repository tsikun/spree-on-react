import React from 'react'
import {Tab,NavBarItem,Preview,PreviewHeader,PreviewBody,PreviewItem,PreviewFooter,PreviewButton} from 'react-weui'
import MessageTopUpItem from './top_up_item'

class MessageTopUps extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <Tab type="navbar" className='msg_nav'>
            <NavBarItem label="待审核">
                <MessageTopUpItem/>
            </NavBarItem>
            <NavBarItem label="已审核">
                <MessageTopUpItem/>
            </NavBarItem>
        </Tab>)
    }
}

export default MessageTopUps;


