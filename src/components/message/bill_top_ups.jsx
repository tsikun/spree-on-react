import React from 'react'
import {Tab,NavBarItem,Preview,PreviewHeader,PreviewBody,PreviewItem,PreviewFooter,PreviewButton} from 'react-weui'
class MessageBillTopUps extends React.Component {
    render(){
        return (<Tab type="navbar">
            <NavBarItem label="待审核">

            </NavBarItem>
            <NavBarItem label="已审核">
                <Preview>
                    <PreviewHeader>
                        <PreviewItem label="Total" value="$49.99" />
                    </PreviewHeader>
                    <PreviewBody>
                        <PreviewItem label="Product" value="Name" />
                        <PreviewItem label="Description" value="Product Description" />
                        <PreviewItem label="Details" value="Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. " />
                    </PreviewBody>
                    <PreviewFooter>
                        <PreviewButton primary>Action</PreviewButton>
                    </PreviewFooter>
                </Preview>
                <ul id="sendDataList2" className="n_l_ul">
                        <li >
                            <div className="n_1_box">
                                <div className="box_bd">
                                    <div className="bd_img fl">
                                        <img alt=""/>
                                    </div>
                                    <div className="bd_txt fl">
                                        <p>
                                            <span>打款人：</span>
                                            <span>user.true_name(user.uname)</span>
                                        </p>
                                        <p >
                                            <span>打款金额：</span>
                                            <span>user.super_id/100元</span>
                                        </p>
                                        <p>
                                            <span>收款人：</span>
                                            <span>user.re_ture_name(user.re_nick_name)</span>
                                        </p>
                                        <p>
                                            <span>备注：</span>
                                            <span>user.msg</span>
                                        </p>
                                        <p>
                                            <span>申请时间：</span>
                                            <span>user.create_time | dateFormat:'yyyy-MM-dd hh:mm'</span>
                                        </p>
                                        <p>
                                            <span>审核状态：</span>
                                            <span>statusMap[user.status]</span>
                                        </p>
                                        <p >
                                            <span>不通过原因：</span>
                                            <span>user.applicant_msg</span>
                                        </p>
                                    </div>
                                </div>
                                <div className="zhang">
                                    <i className="iconfont red">&#xe648;</i>
                                    <i className="iconfont">&#xe64c;</i>
                                </div>
                            </div>
                        </li>
                </ul>

            </NavBarItem>
        </Tab>)
    }
}

export default MessageBillTopUps;


