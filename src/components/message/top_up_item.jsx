import React from 'react'
import {Preview,PreviewHeader,PreviewBody,PreviewItem,PreviewFooter,PreviewButton} from 'react-weui'
import Headimg from '../../images/132.png'

class MessageTopUpItem extends React.Component {
    render(){
        const img=<img className='msg_avatar' src={Headimg}/>;
        return (
                <Preview>
                    <PreviewHeader>
                        <PreviewItem label={img} value="$49.99" />
                    </PreviewHeader>
                    <PreviewBody>
                        <PreviewItem label="打款人" value="Name" />
                        <PreviewItem label="打款金额" value="Product Description" />
                        <PreviewItem label="收款人" value="Product Description" />
                        <PreviewItem label="备注" value="Product Description" />
                        <PreviewItem label="申请时间" value="Product Description" />
                        <PreviewItem label="审核状态" value="Product Description" />
                        <PreviewItem label="不通过原因" value="Product Description" />

                        <i className='icon iconfont icon-shenhejujue1 '/>
                        <i className='icon iconfont icon-caiwushenhe red'/>
                    </PreviewBody>
                    <PreviewFooter>
                        <PreviewButton primary>通过</PreviewButton>
                        <PreviewButton >拒绝</PreviewButton>
                    </PreviewFooter>
                </Preview>
        )
    }
}

export default MessageTopUpItem;