import React from 'react'
import {Article, InfiniteLoader, NavBar, NavBarItem, Tab, TabBody, TabBodyItem} from "react-weui";
import MessageTopUpItem from "../message/top_up_item";
import ReactPaginate from 'react-paginate';

class TopUpHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {tab: 0}
        // this.onLoadMore=this.onLoadMore.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
    }

    onPageChange(data) {

        let currentPage =data .selected;

    }
    componentDidMount() {

    }
    // onLoadMore(finish ,resolve){
    //
    //     setTimeout( ()=> {
    //         if(false){
    //             finish()
    //         }else{
    //             alert ('sex');
    //             resolve();
    //         }
    //     }, 5)
    //
    // }

    render() {
        return (
            <Tab className='msg_nav'>
                <NavBar >
                    <NavBarItem active={this.state.tab == 0} onClick={e => this.setState({tab: 0})}>待审核</NavBarItem>
                    <NavBarItem active={this.state.tab == 1} onClick={e => this.setState({tab: 1})}>已审核</NavBarItem>
                </NavBar>
                <TabBody>
                    <TabBodyItem active={true}>
                        <MessageTopUpItem/>
                        <MessageTopUpItem/>
                        <ReactPaginate pageCount={15}
                                       marginPagesDisplayed={3}
                                       initialPage={0}
                                       onPageChange={this.onPageChange}
                                       pageRangeDisplayed={5}
                                       previousLabel={"<<"}
                                       nextLabel={">>"}
                                       breakLabel={"..."}
                        />
                    </TabBodyItem>
                </TabBody>
            </Tab>

        )
    }
}

export default TopUpHistory;