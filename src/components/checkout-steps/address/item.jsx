import React, {Component} from 'react';
import {Page,MediaBox, MediaBoxDescription, MediaBoxTitle, MediaBoxInfo,Panel,PanelBody,PanelFooter,PanelHeader,Button,Cell,CellBody,CellHeader,CellFooter} from "react-weui";

class AddressItem extends Component {

    constructor(props) {
        super(props);
        this.handleCountryChange = this.handleCountryChange.bind(this);
    };

    handleCountryChange(event) {
        this.props.handleCountryChange(event.currentTarget.value);
        // Trigger the redux-form onChange callback.
        this.props.input.onChange(event.currentTarget.value);
    };

    render() {
        // let countryOptionsMarkup = this.props.countries.map((country, idx) => {
        //   return (
        //     <option key={ idx } value={ country.id } >
        //       { country.name }
        //     </option>
        //   );
        // });

        return (
            <Page>

                <Panel className='address-item address-default'>
                    <PanelBody>
                        <Cell access>
                            <CellBody>
                        <MediaBox type="text">
                            <MediaBoxTitle>
                                <i className="icon iconfont icon-huiyuangl icon_margin">姓名</i>
                                &nbsp;&nbsp;
                                <i className="icon iconfont icon-shouji1 icon_margin">12327283744</i>
                                &nbsp;&nbsp;
                                <Button size="small" disabled className='default-button'>默认地址</Button>
                            </MediaBoxTitle>
                            <MediaBoxDescription>地址</MediaBoxDescription>
                        </MediaBox>
                        </CellBody>
                            <CellFooter/></Cell>
                    </PanelBody>

                </Panel>
            <Panel className='address-item address-default'>
                <PanelBody>
                    <MediaBox type="text">
                    <MediaBoxTitle>
                        <i className="icon iconfont icon-huiyuangl icon_margin">姓名</i>
                        &nbsp;&nbsp;
                        <i className="icon iconfont icon-shouji1 icon_margin">12327283744</i>
                        &nbsp;&nbsp;
                        <Button size="small" disabled className='default-button'>默认地址</Button>
                    </MediaBoxTitle>
                    <MediaBoxDescription>地址</MediaBoxDescription>
                </MediaBox>
                </PanelBody>
                <PanelFooter>
                    <Cell link className='command-area'>
                        <CellBody> <Button size="small" type="primary" plain className='set-button'>设置为默认地址</Button></CellBody>
                        <CellFooter><a><i className="icon iconfont icon-bianji1 icon_margin command-area">edit</i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a><i className="icon iconfont icon-shanchu1 icon_margin command-area">Del</i></a></CellFooter>
                    </Cell>
                   </PanelFooter>
            </Panel>
                <Panel className='address-item'>
                    <PanelBody>
                        <MediaBox type="text">
                            <MediaBoxTitle>
                                <i className="icon iconfont icon-huiyuangl icon_margin">姓名</i>
                                &nbsp;&nbsp;
                                <i className="icon iconfont icon-shouji1 icon_margin">12327283744</i>
                                &nbsp;&nbsp;
                            </MediaBoxTitle>
                            <MediaBoxDescription>地址</MediaBoxDescription>
                        </MediaBox>
                    </PanelBody>
                    <PanelFooter>
                        <Cell link className='command-area'>
                            <CellBody> <Button size="small" type="primary" plain className='set-button'>设置为默认地址</Button></CellBody>
                            <CellFooter><a><i className="icon iconfont icon-bianji1 icon_margin command-area">edit</i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a><i className="icon iconfont icon-shanchu1 icon_margin command-area">Del</i></a></CellFooter>
                        </Cell>
                    </PanelFooter>
                </Panel>

                </Page>
        );
    };
};

export default AddressItem;
