import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import {
    Panel,
    PanelHeader,
    PanelBody,
    PanelFooter,
    MediaBox,
    MediaBoxHeader,
    MediaBoxBody,
    MediaBoxTitle,
    MediaBoxDescription,
    MediaBoxInfo,
    MediaBoxInfoMeta,
    Cells,
    Cell,
    CellHeader,
    CellBody,
    CellFooter, Switch, FormCell, Form, TextArea, Flex, FlexItem, Button
} from "react-weui";
import { FormattedMessage } from 'react-intl';

import Layout from "../layout";
import BaseCheckoutLayout from "./base-checkout-layout";
import CheckoutStepCalculator from '../../services/checkout-step-calculator';

import Address from '../order/address';
import LineItem from '../order/line-item';
import MessagePage from "../message-page";

class ConfirmationForm extends Component {

  /* Render this step only if order is present and in a valid checkout state. */
  componentWillMount() {
    let order = this.props.order;

    if (!CheckoutStepCalculator.isStepEditable(order.checkout_steps, 'confirm', order.state)){
      this.props.handleCheckoutStepNotEditable(order, this.props.placedOrder);
    }
  };

  componentDidMount() {
    this.props.setCurrentCheckoutStep();
  };

  handleFormSubmit (formData) {
    this.props.handleFormSubmit(formData, this.props.order);
  };

  _shipmentLineItemsMarkup () {
    let thisShipment = this.props.order.shipments[0];
    let shipmentLineItems = this.props.order.line_items.filter((lineItem) => {
      return (lineItem.variant_id !== thisShipment.manifest.variant_id);
    });
    return shipmentLineItems.map((lineItem, idx) => {
      return <LineItem lineItem={ lineItem } key={ idx } />
    });
  };


  render() {
    // const { handleSubmit, valid, submitting } = this.props;
    return (
        <div >
          <MediaBox type="text">
              <MediaBoxTitle>姓名</MediaBoxTitle>
              <MediaBoxDescription>地址</MediaBoxDescription>
          </MediaBox>
            <MediaBox type="appmsg" href="javascript:void(0);">
            <MediaBoxHeader></MediaBoxHeader>
            <MediaBoxBody>
                <MediaBoxTitle>Media heading</MediaBoxTitle>
                <MediaBoxDescription>
                    嘿嘿
                </MediaBoxDescription>
            </MediaBoxBody>
        </MediaBox>
            <Cells>
                <Cell>
                    <CellHeader>支付方式</CellHeader>
                    <CellBody>微信支付</CellBody>
                </Cell>
                <FormCell>
                    <CellHeader>积分抵扣</CellHeader>
                    <CellBody>多少积分可抵扣</CellBody>
                    <CellFooter>
                        <Switch/>
                    </CellFooter>
                </FormCell>
            </Cells>
            <Cells>
                <Cell>
                    <CellHeader>商品金额</CellHeader>
                    <CellBody/>
                    <CellFooter>1000.00</CellFooter>
                </Cell>
                <Cell>
                    <CellHeader>优惠金额</CellHeader>
                    <CellBody/>
                    <CellFooter>
                        25254
                    </CellFooter>
                </Cell>
            </Cells>
            <Form>
                <FormCell>
                    <CellBody>
                        <TextArea placeholder="选填: 给我们留言" rows="3" maxlength="100"></TextArea>
                    </CellBody>
                </FormCell>
            </Form>
            <div className="buycart_submit bottom-fixed">
                <Flex >
                    <FlexItem className="">
                        <Form checkbox style={{margin: 0,border:0}}>
                            <FormCell checkbox>
                                <CellHeader>
                                </CellHeader>
                                <CellBody>全选</CellBody>
                            </FormCell>
                        </Form>
                    </FlexItem>
                    <FlexItem className="flex_sum">
                        <p> 总计: ￥ <em>0.00</em></p>
                        <p>(不含运费)</p>
                    </FlexItem>
                    <FlexItem >
                        <Button  style={{'border-radius': "0"}}>去结算</Button>
                    </FlexItem>
                </Flex>
            </div>
        </div>
    );
  };
};

ConfirmationForm = reduxForm({
  form: 'confirmationForm'
})(ConfirmationForm);

export default ConfirmationForm;
