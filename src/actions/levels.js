import APP_ACTIONS from '../constants/app-actions';

const levels = {
  addLevels: (levels) => {
    return {
      type: APP_ACTIONS.ADD_LEVELS,
      payload: levels
    }
  }
};

export default levels;
