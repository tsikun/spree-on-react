import APP_ACTIONS from '../constants/app-actions';
import { push } from 'react-router-redux';
import AuthorizeAgentsAPI from '../apis/authorize-agents'
import Actions from './'
import {tokenForAPI} from "./utils";
import APP_ROUTES from "../constants/app-routes";

const authorizeAgents = {
    addAuthorizeAgents: (response) => {
        return {
            type: APP_ACTIONS.ADD_AUTHORIZE_AGENTS,
            payload: response
        }
    },
    applyAuthorizeAgents: (formData) => {
        return (dispatch, getState) => {
            dispatch(Actions.submitForm());
            let tokenParam = getState().user.token;
            //call api
            return AuthorizeAgentsAPI.create(tokenParam, formData).then(res => {
                    dispatch(Actions.addAuthorizeAgents(res.body));
                    dispatch(Actions.notSubmitForm());
                    dispatch(push(APP_ROUTES.workbench.qrCodePageRoute));
                },
                (err) => {
                 // console.log(err)
                    dispatch(Actions.notSubmitForm())
                }
            )
        }
    },
    updateAuthorizeAgents: ()=> {
        return (dispatch,getState)=> {

        }
    }
};

export default authorizeAgents;
