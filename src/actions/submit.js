import APP_ACTIONS from '../constants/app-actions';

const submit = {
  submitForm: () => {
    return {
      type: APP_ACTIONS.SUBMIT_FORM,
    };
  },

  notSubmitForm: () => {
    return {
      type: APP_ACTIONS.NOTSUBMIT_FORM,
    }
  }
};

export default submit;
