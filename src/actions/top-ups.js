import APP_ACTIONS from '../constants/app-actions';
import {push} from 'react-router-redux';
import TopUpAPI from '../apis/top_up';
import Actions from './'
import {tokenForAPI} from "./utils";
import APP_ROUTES from "../constants/app-routes";
import TaxonFinder from "../services/taxon-finder";

const topUps = {
    addMsgTopUps: (response) => {
        return {
            type: APP_ACTIONS.ADD_MSG_TOP_UPS,
            payload: response
        }
    },
    createTopUp: (formData) => {
        return (dispatch, getState) => {
            dispatch(Actions.submitForm());
            let tokenParam = {token: getState().user.token};
            //call api
            return TopUpAPI.create(tokenParam, formData).then(res => {
                    // dispatch(Actions.addAuthorizeAgents(res.body));
                    dispatch(Actions.notSubmitForm());
                    dispatch(push(APP_ROUTES.finance.topUpHistoryRoute));
                },
                (err) => {
                    // console.log(err)
                    dispatch(Actions.notSubmitForm())
                }
            )
        }
    },
    updateAuthorizeAgents: () => {
        return (dispatch, getState) => {

        }
    },
    getTopUps: () => {
        return (dispatch, getState) => {

        }
    },

    addProducts: (productsResponse) => {
        return {
            type: APP_ACTIONS.ADD_PRODUCTS,
            payload: productsResponse
        };
    },

    appendProducts: (productsResponse) => {
        return {
            type: APP_ACTIONS.APPEND_PRODUCTS,
            payload: productsResponse
        };
    },

    addProduct: (product) => {
        return {
            type: APP_ACTIONS.ADD_PRODUCT,
            payload: product
        }
    },

    fetchTopUps: (paramsToMerge = {}) => {
        return (dispatch, getState) => {
            let tokenParam = {token: getState().user.token};


            return TopUpAPI.getList(tokenParam,paramsToMerge);
        }
    }

};

export default topUps;
